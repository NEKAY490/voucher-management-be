const knex = require('knex');
const knexfile = require('./knexfile');
const pool = knex(knexfile.development);

module.exports = pool;
