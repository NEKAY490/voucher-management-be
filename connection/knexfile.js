var conf = require("../config.json");

module.exports = {
    development: {
        client: "pg",
        connection: {
            host: conf.dbConfig.db.host,
            database: conf.dbConfig.db.database,
            user: conf.dbConfig.db.user,
            password: conf.dbConfig.db.password,
            port: conf.dbConfig.db.port,
            ssl: { require: false, rejectUnauthorized: false }
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: "pgmigra",
        },
        seeds: {
            directory: './seeds',
        },
    },
};
