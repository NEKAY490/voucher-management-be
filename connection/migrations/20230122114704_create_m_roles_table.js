
exports.up = function(knex) {
  return knex.schema.createTable("m_roles", (table) => {
    table.increments("id").primary();
    table.string('name');
    table.string('modules_id');
    table.integer('id_merchant')
    table.integer('created_by');
    table.integer('updated_by');
    table.timestamp('created_at').defaultTo(knex.fn.now())
    table.timestamp('updated_at').defaultTo(knex.fn.now())
    table.timestamp('deleted_at')
  });
};

exports.down = function(knex) {
  return knex.schema.dropTable('m_roles');
};
