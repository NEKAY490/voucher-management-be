exports.up = function (knex) {
  return knex.schema.createTable("m_modules", (table) => {
    table.increments("id").primary();
    table.string('name');
    table.string('slug');
    table.string('url');
    table.string('icon');
    table.timestamp('created_at').defaultTo(knex.fn.now())
    table.timestamp('updated_at').defaultTo(knex.fn.now())
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('m_modules');
};
