exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex("m_modules")
    .del()
    .then(function () {
      // Inserts seed entries
      return knex("m_modules").insert([
        {
          name: "Member",
          slug: "member",
          url: "/member",
          icon: "communication/com013.svg",
        },
        {
          name: "Roles",
          slug: "roles",
          url: "/roles",
          icon: "general/gen019.svg",
        },
        {
          name: "Activity Log",
          slug: "activity-log",
          url: "/activity-log",
          icon: "general/gen005.svg",
        },
        {
          name: "API Warehouse",
          slug: "api-warehouse",
          url: "/api-warehouse",
          icon: "coding/cod002.svg",
        },
        {
          name: "Package",
          slug: "package",
          url: "/package",
          icon: "general/gen017.svg",
        },
        {
          name: "Partner",
          slug: "voucher-partner",
          url: "/voucher/partner",
          icon: "arrows/arr076.svg",
        },
        {
          name: "Voucher Internal Customer",
          slug: "voucher-internal-customer",
          url: "/voucher/internal/customer",
          icon: "arrows/arr077.svg",
        },
        {
          name: "Voucher Internal Business",
          slug: "voucher-internal-business",
          url: "/voucher/internal/business",
          icon: "arrows/arr077.svg",
        },
        {
          name: "Claims Voucher Customer",
          slug: "voucher-claims-customer",
          url: "/voucher/claims/customer",
          icon: "ecommerce/ecm008.svg",
        },
        {
          name: "Claims Voucher Customer",
          slug: "voucher-claims-business",
          url: "/voucher/claims/business",
          icon: "ecommerce/ecm008.svg",
        },
      ]);
    });
};
