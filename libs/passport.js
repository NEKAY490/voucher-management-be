const passport = require("passport");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");
const config = require("../config.json");
const knex = require("../connection/db");

/* Passport JWT Options */
const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.jwtSecret,
};

passport.use(
  new JwtStrategy(options, async (payload, done) => {
    knex("m_account")
      .where({ id: payload.id })
      .first()
      .then((user) => done(null, user))
      .catch((err) => done(err, false));
  })
);

module.exports = passport;
