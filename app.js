var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors')

const routes = require('./routes/index');

//utis
// var uploadRouter = require('./routes/utils/serv_upload');

var app = express();
app.use(cors())

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// logging
const knex = require("./connection/db");
const { getAccountFromToken, getLogDescription } = require("./utils");

const activity = (req, res, next) => {
  res.on("finish", async () => {
    const { originalUrl, method, body } = req;

    const methodsTrack = ['POST', 'PUT', 'DELETE'];
    if (methodsTrack.includes(method) && res.statusCode === 200)
    {
      const { id_merchant } = body
      const account = await getAccountFromToken(req);

      const description = getLogDescription(method, originalUrl, body);
      const payload = {
        values_json: JSON.stringify(body),
        id_merchant,
        description,
        created_by: +account.id,
      };

      // Add logging to DB
      const respLogging = await knex("trx_logging")
        .insert(payload)
        .returning(["id"]);
    }
  });

  next();
}
app.use(activity)

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// health-check
app.get('/', function(req, res, next) {
  res.status(200).json({
    data: "healthy!"
  });
});

app.use('/v1/api/', routes);
// app.use('/v1/api/utils', uploadRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
