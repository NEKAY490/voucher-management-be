const jwt = require('jsonwebtoken');
const config = require("../config.json");
const { getAccountFromToken, getModulesById } = require("../utils");

module.exports = (permission) => async (req, res, next) => {
  const account = await getAccountFromToken(req);
  const modules = await getModulesById(account.id_group)

  const isMatchRole = modules.some((module) => module.slug === permission);
  if (isMatchRole) {
    return next();
  }

  res.status(403).json({
    status: '403',
      message: 'You do not have the authorization to access this',
  });
};
