var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");
const bcrypt = require('bcrypt');
var salt = bcrypt.genSaltSync(10);

module.exports = router;

router.get("/", async (req, res, next) => {
    try {
        const page = +req.query.page || 1
        const per_page = +req.query.per_page || 10

        const query = conn.from("m_merchant")

        const data = await query
            .clone()
            .select()
            .orderBy("name_merchant", "ASC")
            .limit(per_page)
            .offset((page - 1) * per_page)

        const totalCount = await query
            .clone()
            .count()
            .first()


        if (data.length > 0) {
            res.status(200).json({
                status: 200,
                data,
                meta: {
                    count: data.length,
                    current_page: page,
                    per_page,
                    total: +totalCount.count,
                    total_pages: Math.ceil(totalCount.count / per_page),
                },
            });
        } else {
            res.status(204).json({
                status: 204,
                data: {},
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.get("/filter", async (req, res, next) => {
    try {

        let name_merchant = req.query.name_merchant
        const page = +req.query.page || 1
        const per_page = +req.query.per_page || 10

        const query = conn
            .select("m_merchant.*",
                "m_account.id as id_acc",
                "m_account.name_account",
                "m_account.mail_account",
                "m_account.password",
                "m_pic.id as id_pic",
                "m_pic.name_pic",
                "m_pic.phone_pic",
                "m_pic.mail_pic",
            )
            .from("m_merchant")
            .innerJoin("m_account", "m_account.id_merchant", "m_merchant.id")
            .innerJoin("m_pic", "m_pic.id_merchant", "m_merchant.id")
            .orderBy("m_merchant.name_merchant", "ASC")
            .modify(function (queryBuilder) {
                if (name_merchant) {
                    queryBuilder.whereRaw("lower(name_merchant) like lower(?)", [`%${name_merchant}%`]);
                }
            })

        const data = await query
            .clone()
            .limit(per_page)
            .offset((page - 1) * per_page);

        const totalCount = await query
            .clone();

        if (data.length > 0) {
            res.status(200).json({
                status: 200,
                data,
                meta: {
                    count: data.length,
                    current_page: page,
                    per_page,
                    total: totalCount.length,
                    total_pages: Math.ceil(totalCount.length / per_page),
                },
            });
        } else {
            res.status(204).json({
                status: 204,
                data: "Data not found",
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});


router.get("/:id", async (req, res, next) => {
    try {

        let id = req.params.id

        const dataMerchant = await conn
            .select("m_merchant.*",
                "m_account.id as id_acc",
                "m_account.name_account",
                "m_account.mail_account",
                "m_account.password",
                "m_pic.id as id_pic",
                "m_pic.name_pic",
                "m_pic.phone_pic",
                "m_pic.mail_pic",
            )
            .from("m_merchant")
            .innerJoin("m_account", "m_account.id_merchant", "m_merchant.id")
            .innerJoin("m_pic", "m_pic.id_merchant", "m_merchant.id")
            .where("m_merchant.id", id)

        if (dataMerchant.length > 0) {
            res.status(200).json({
                status: 200,
                data: dataMerchant,
            });
        } else {
            res.status(204).json({
                status: 204,
                data: "Data not found",
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});


router.post("/", async (req, res, next) => {
    try {

        const {
            name_merchant,
            address_merchant,
            phone_merchant,
            mail_merchant,
            url_merchant,
            desc_merchant,
            name_pic,
            phone_pic,
            mail_pic,
            name_account,
            mail_account,
            password,
        } = req.body;

        var password_hash = bcrypt.hashSync(password, salt);

        console.log(password_hash)

        const NameExist = await conn
            .select("*")
            .from("m_merchant")
            .andWhere("name_merchant", name_merchant)

        if (NameExist.length > 0) {
            res.status(200).json({
                status: 409,
                data: "Merchant Data Already Exist",
            });
        } else {
            const respMerchant = await conn("m_merchant")
                .insert({
                    name_merchant: name_merchant,
                    address_merchant: address_merchant,
                    phone_merchant: phone_merchant,
                    mail_merchant: mail_merchant,
                    url_merchant: url_merchant,
                    desc_merchant: desc_merchant,
                })
                .returning(["id"]);

            // create new roles for admin merchant with full access
            const getModules = await conn("m_modules").select(["id"]);
            const modulesId = getModules.map((module) => module.id).toString();
            const createdRoles = await conn("m_roles")
            .insert({
                name: 'Admin',
                modules_id: modulesId,
                id_merchant: respMerchant[0].id,
                created_by: '1', // 1 is superadmin
                created_at: conn.fn.now(),
            })
            .returning(["id"]);

            const NameExist = await conn
                .select("*")
                .from("m_account")
                .andWhere("mail_account", mail_account)

            if (NameExist.length > 0) {
                res.status(200).json({
                    status: 409,
                    data: "Email Data Already Exist",
                });
            } else {
                const respAccount = await conn("m_account")
                    .insert({
                        name_account: name_account,
                        mail_account: mail_account,
                        password: password_hash,
                        id_group: createdRoles[0].id,
                        id_merchant: respMerchant[0].id,
                    })
                    .returning(["id"]);

                const respPIC = await conn("m_pic")
                    .insert({
                        name_pic: name_pic,
                        phone_pic: phone_pic,
                        mail_pic: mail_pic,
                        id_merchant: respMerchant[0].id,

                    })
                    .returning(["id"]);
            }



            if (respMerchant.length > 0) {
                console.log("Success Insert data");
                res.status(200).json({
                    status: 200,
                    data: "Data has been inserted",
                });
            } else {
                console.log("Failed Insert data");
                res.status(500).json({
                    status: 500,
                    data: "Failed insert data",
                });
            }
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.put("/:id", async (req, res, next) => {
    try {

        let id = req.params.id

        const {
            id_pic,
            id_acc,
            name_merchant,
            address_merchant,
            phone_merchant,
            mail_merchant,
            url_merchant,
            desc_merchant,
            name_pic,
            phone_pic,
            mail_pic,
            name_account,
            mail_account,
            password,
        } = req.body;

        var password_hash = bcrypt.hashSync(password, salt);

        const respPIC = await conn("m_pic")
            .update({
                name_pic: name_pic,
                phone_pic: phone_pic,
                mail_pic: mail_pic,
                updated_at:conn.fn.now()
            })
            .where("id", id_pic)
            .returning(["id"]);

        const respAccount = await conn("m_account")
            .update({
                name_account: name_account,
                mail_account: mail_account,
                password: password_hash,
                updated_at:conn.fn.now()
            })
            .where("id", id_acc)
            .returning(["id"]);

        console.log(respPIC)
        console.log(respAccount)

        const respMerchant = await conn("m_merchant")
            .update({
                name_merchant: name_merchant,
                address_merchant: address_merchant,
                phone_merchant: phone_merchant,
                mail_merchant: mail_merchant,
                url_merchant: url_merchant,
                desc_merchant: desc_merchant,
                updated_at:conn.fn.now()
            })
            .where("id", id)
            .returning(["id"]);

        if (respMerchant.length > 0) {
            console.log("Success Insert data");
            res.status(200).json({
                status: 200,
                data: "Data has been updated",
            });
        } else {
            console.log("Failed Insert data");
            res.status(500).json({
                status: 500,
                data: "Failed insert data",
            });
        }

        // const NameExist = await conn
        //     .select("*")
        //     .from("m_merchant")
        //     .andWhere("name_merchant", name_merchant)
        //
        // if (NameExist.length > 0) {
        //     res.status(200).json({
        //         status: 409,
        //         data: "Data Already Exist",
        //     });
        // } else {
        //
        // }

} catch (e) {
    console.log(e)
    res.status(500).json({
        status: 500,
        data: "Internal Server Error",
    });
}

});

router.delete("/:id", async (req, res, next) => {
    let id = req.params.id;

    try {
        let resp = await conn("m_merchant")
            .where("id", id)
            .del();

        res.status(200).json({
            status: 200,
            data: "Success delete data",
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 500,
            data: "Error delete data",
        });
    }
});
