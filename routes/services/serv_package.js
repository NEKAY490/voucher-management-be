var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");

module.exports = router;

router.get("/", async (req, res, next) => {

    let id_merchant = req.query.id_merchant;
    let code_package = req.query.code_package;
    let name_package = req.query.name_package;
    let page = +req.query.page || 1
    let per_page = +req.query.per_page || 10


    try {

        const query = conn("m_package")
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("id_merchant", id_merchant);
                }
                if (code_package) {
                    queryBuilder.whereRaw("lower(code_package) like lower(?)", [`%${code_package}%`]);
                }
                if (name_package) {
                    queryBuilder.whereRaw("lower(name_package) like lower(?)", [`%${name_package}%`]);
                }
            })

        const data = await query
            .clone()
            .select()
            .orderBy ("created_at", "desc")
            .limit(per_page)
            .offset((page - 1) * per_page)

        const totalCount = await query
            .clone()
            .count()
            .first()

        res.status(200).json({
            status: 200,
            data,
            meta: {
                count: data.length,
                current_page: page,
                per_page,
                total: totalCount.count,
                total_pages: Math.ceil(totalCount.count / per_page),
            },
        });

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.get("/:id", async (req, res, next) => {
    try {

        let id = req.params.id

        const dataVoucher = await conn
            .select("*")
            .from("m_package")
            .where("id", id)

        if (dataVoucher.length > 0) {
            res.status(200).json({
                status: 200,
                data: dataVoucher,
            });
        } else {
            res.status(204).json({
                status: 204,
                data: "Data not found",
            });
        }

    } catch (e) {
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.post("/", async (req, res, next) => {
    try {

        const {
            name_package,
            code_package,
            desc_package,
            id_merchant,
            id_api_warehouse,

        } = req.body;

        const NameExist = await conn
            .select("*")
            .from("m_package")
            .andWhere("code_package", code_package)

        if (NameExist.length > 0) {
            res.status(409).json({
                status: 409,
                data: "Data Already Exist",
            });
        } else {

            const respMPackage = await conn("m_package")
                .insert({
                    name_package: name_package,
                    code_package: code_package,
                    desc_package: desc_package,
                    id_merchant: id_merchant,
                    id_api_warehouse: id_api_warehouse,
                })
                .returning(["*"]);

            if (respMPackage.length > 0) {
                console.log("Success Insert data");
                res.status(200).json({
                    status: 200,
                    data: "Data has been inserted",
                });
            } else {
                console.log("Failed Insert data");
                res.status(500).json({
                    status: 500,
                    data: "Failed insert data",
                });
            }
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.put("/:id", async (req, res, next) => {
    try {

        let id = req.params.id

        const {
            code_package,
            name_package,
            desc_package,
            id_api_warehouse,

        } = req.body;

        const NameExist = await conn
            .select("*")
            .from("m_package")
            .andWhere("code_package", code_package)

        if (NameExist.length > 0) {
            res.status(409).json({
                status: 409,
                data: "Data Already Exist",
            });
        } else {
            const respPackage = await conn("m_package")
                .update({
                    code_package: code_package,
                    name_package: name_package,
                    desc_package: desc_package,
                    id_api_warehouse: id_api_warehouse,
                    updated_at:conn.fn.now()
                })
                .where("id", id)
                .returning(["id"]);

            if (respPackage.length > 0) {
                console.log("Success Insert data");
                res.status(200).json({
                    status: 200,
                    data: "Data has been updated",
                });
            } else {
                console.log("Failed Insert data");
                res.status(500).json({
                    status: 500,
                    data: "Failed insert data",
                });
            }
        }



    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.delete("/", async (req, res, next) => {

    let id = req.body.id;


    try {
        let resp = await conn("m_package")
            .del()
            .whereIn("id", id)

        res.status(200).json({
            status: 200,
            data: "Success delete data",
        });


    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 500,
            data: "Error delete data",
        });
    }
});
