var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");

module.exports = router;


router.get("/", async (req, res, next) => {
    try {

        let id_merchant = req.query.id_merchant
        const page = +req.query.page || 1
        const per_page = +req.query.per_page || 10

        const query = conn
            .select("trx_logging.values_json",
                "trx_logging.description",
                "trx_logging.created_at",
                "trx_logging.updated_at",
                "m_account.name_account")
            .from ("trx_logging")
            .leftJoin("m_account", "trx_logging.created_by", "m_account.id")
            .where("trx_logging.id_merchant", id_merchant)
            .groupBy("trx_logging.id", "m_account.name_account")


        const data = await query
            .clone()
            .select()
            .orderBy ("trx_logging.created_at", "desc")
            .limit(per_page)
            .offset((page - 1) * per_page)

        const totalCount = await query

        res.status(200).json({
            status: 200,
            data,
            meta: {
                count: data.length,
                current_page: page,
                per_page,
                total: totalCount.length,
                total_pages: Math.ceil(totalCount.length / per_page),
            },
        });

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.post("/", async (req, res, next) => {
    try {

        const {
            id_merchant,
            values_json,
            description,
            created_by,
        } = req.body;

        const respLogging = await conn("trx_logging")
            .insert({
                id_merchant: id_merchant,
                values_json: values_json,
                description: description,
                created_by: created_by,

            })
            .returning(["id"]);

        if (respLogging.length > 0) {
            console.log("Success Insert data");
            res.status(200).json({
                status: 200,
                data: "Data has been inserted",
            });
        } else {
            console.log("Failed Insert data");
            res.status(500).json({
                status: 500,
                data: "Failed insert data",
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});
