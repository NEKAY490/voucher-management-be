var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");
const bcrypt = require("bcrypt");

module.exports = router;

router.get("/", async (req, res, next) => {

    let id_merchant = req.query.id_merchant;
    let page = +req.query.page || 1
    let per_page = +req.query.per_page || 10

    try {
        const query = conn("m_account")
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("id_merchant", id_merchant);
                }
            })

        const data = await query
            .clone()
            .select()
            .limit(per_page)
            .offset((page - 1) * per_page)

        const totalCount = await query
            .clone()
            .count()
            .first()

        res.status(200).json({
            status: 200,
            data,
            meta: {
                count: data.length,
                current_page: page,
                per_page,
                total: totalCount.count,
                total_pages: Math.ceil(totalCount.count / per_page),
            },
        });

    } catch (e) {
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.get("/:id", async (req, res, next) => {
    try {

        let id = req.params.id

        const dataUser = await conn
            .select("*")
            .from("m_account")
            .where("id", id)

        if (dataUser.length > 0) {
            res.status(200).json({
                status: 200,
                data: dataUser,
            });
        } else {
            res.status(204).json({
                status: 204,
                data: "Data not found",
            });
        }

    } catch (e) {
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.post("/", async (req, res, next) => {
    try {

        const {
            name_account,
            password,
            mail_account,
            phone_account,
            account_status,
            id_group,
            id_merchant,
            created_by,
        } = req.body;

        var password_hash = bcrypt.hashSync(password, salt);

        console.log(password_hash)

        const respUser = await conn("m_merchant")
            .insert({
                name_account: name_account,
                mail_account: mail_account,
                password: password_hash,
                phone_account: phone_account,
                mail_merchant: mail_merchant,
                account_status: account_status,
                id_group: id_group,
                id_merchant: id_merchant,
                created_by: created_by,
            })
            .returning(["id"]);

        if (respUser.length > 0) {
            console.log("Success Insert data");
            res.status(200).json({
                status: 200,
                data: "Data has been inserted",
            });
        } else {
            console.log("Failed Insert data");
            res.status(500).json({
                status: 500,
                data: "Failed insert data",
            });
        }

    } catch (e) {
        console.log('create user', e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.put("/:id", async (req, res, next) => {
    try {

        let id = req.params.id

        const {
            name_account,
            password,
            mail_account,
            phone_account,
            account_status,
            id_group,
            updated_by,
        } = req.body;

        var password_hash = bcrypt.hashSync(password, salt);

        const respMerchant = await conn("m_merchant")
            .update({
                name_account: name_account,
                password: password_hash,
                mail_account: mail_account,
                phone_account: phone_account,
                account_status: account_status,
                id_group: id_group,
                updated_by: updated_by,
                updated_at:conn.fn.now()
            })
            .where("id", id)
            .returning(["id"]);

        if (respMerchant.length > 0) {
            console.log("Success Insert data");
            res.status(200).json({
                status: 200,
                data: "Data has been updated",
            });
        } else {
            console.log("Failed Insert data");
            res.status(500).json({
                status: 500,
                data: "Failed insert data",
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.delete("/:id", async (req, res, next) => {
    let id = req.body.id;

    try {
        let resp = await conn("m_account")
            .whereIn("id", id)
            .del();

        res.status(200).json({
            status: 200,
            data: "Success delete data",
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 500,
            data: "Error delete data",
        });
    }
});
