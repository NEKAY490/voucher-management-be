var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");

module.exports = router;

router.get("/getbypackage", async (req, res, next) => {
    try {

        let id_package = req.query.id_package;

        const getByPackage = await conn
            .select("id", "name_batch")
            .from("m_batch")
            .where("id_package", id_package)

        if (getByPackage.length > 0) {
            res.status(200).json({
                status: 200,
                data: getByPackage,
            });
        } else {
            res.status(204).json({
                status: 204,
                data: "Data not found",
            });
        }

    } catch (error) {
        console.log(error)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});


router.get("/business/getAll", async (req, res, next) => {

    let id_merchant = req.query.id_merchant
    let type = req.query.type
    const page = +req.query.page || 1
    const per_page = +req.query.per_page || 10

    try {

        const query = conn
            .select("m_voucher.id_batch", "m_batch.name_batch", "m_package.name_package", "m_batch.created_at",
                "m_voucher.expired_date", "m_batch.total")
            .from("m_voucher")
            .innerJoin("m_batch", "m_voucher.id_batch", "m_batch.id")
            .innerJoin("m_package", "m_voucher.id_package", "m_package.id")
            .groupBy("m_voucher.id_batch",  "m_batch.id", "m_batch.name_batch", "m_package.name_package", "m_voucher.expired_date")
            .orderBy("m_batch.created_at", "desc")
            .where("m_batch.type", "2")
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("m_voucher.id_merchant", id_merchant);
                }
            })

        const data = await query
            .clone()
            .limit(per_page)
            .offset((page - 1) * per_page);

        const totalCount = await query
            .clone();

        res.status(200).json({
            status: 200,
            data,
            meta: {
                count: data.length,
                current_page: page,
                per_page,
                total: totalCount.count,
                total_pages: Math.ceil(totalCount.count / per_page),
            },
        });

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.get("/", async (req, res, next) => {
    try {

        const page = +req.query.page || 1
        const per_page = +req.query.per_page || 10

        const query = conn.from("m_batch")

        const data = await query
            .clone()
            .select()
            .limit(per_page)
            .offset((page - 1) * per_page)

        const totalCount = await query
            .clone()
            .count()
            .first()

        if (data.length > 0) {
            res.status(200).json({
                status: 200,
                data,
                meta: {
                    count: data.length,
                    current_page: page,
                    per_page,
                    total: +totalCount.count,
                    total_pages: Math.ceil(totalCount.count / per_page),
                },
            });
        } else {
            res.status(204).json({
                status: 204,
                data: {},
            });
        }

    } catch (e) {
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.get("/:id", async (req, res, next) => {
    try {

        let id = req.params.id

        const dataVoucher = await conn
            .select("*")
            .from("m_batch")
            .where("id", id)

        if (dataVoucher.length > 0) {
            res.status(200).json({
                status: 200,
                data: dataVoucher,
            });
        } else {
            res.status(204).json({
                status: 204,
                data: "Data not found",
            });
        }

    } catch (e) {
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});


router.post("/", async (req, res, next) => {
    try {

        const {
            name_batch,
            total,
        } = req.body;

        const respAPIWarehouse = await conn("m_batch")
            .insert({
                name_batch: name_batch,
                total: total,
            })
            .returning(["id"]);

        if (respAPIWarehouse.length > 0) {
            console.log("Success Insert data");
            res.status(200).json({
                status: 200,
                data: "Data has been inserted",
            });
        } else {
            console.log("Failed Insert data");
            res.status(500).json({
                status: 500,
                data: "Failed insert data",
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.put("/:id", async (req, res, next) => {
    try {

        let id = req.params.id

        const {
            name_batch,
            total,
        } = req.body;

        const respPackage = await conn("m_batch")
            .update({
                name_batch: name_batch,
                total: total,
                updated_at:conn.fn.now()
            })
            .where("id", id)
            .returning(["id"]);

        if (respPackage.length > 0) {
            console.log("Success Insert data");
            res.status(200).json({
                status: 200,
                data: "Data has been updated",
            });
        } else {
            console.log("Failed Insert data");
            res.status(500).json({
                status: 500,
                data: "Failed insert data",
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.delete("/", async (req, res, next) => {
    let id = req.body.id;

    try {
        let resp = await conn("m_batch")
            .whereIn("id", id)
            .del();

        let del = await conn("m_voucher")
            .whereIn("id_batch", id)
            .del();

        res.status(200).json({
            status: 200,
            data: "Success delete data",
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 500,
            data: "Error delete data",
        });
    }
});

//get all voucher for bisnis
