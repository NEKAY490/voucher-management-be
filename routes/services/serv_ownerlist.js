var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");

module.exports = router;


router.get("/", async (req, res, next) => {

    let id_merchant = req.query.id_merchant;

    try {
        const dataAPI = await conn("m_ownerlist")
            .select("*")
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("id_merchant", id_merchant);
                }
            })

        if (dataAPI.length > 0) {
            res.status(200).json({
                status: 200,
                data: dataAPI,
            });
        } else {
            res.status(204).json({
                status: 204,
                data: {},
            });
        }

    } catch (e) {
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.get("/:id", async (req, res, next) => {
    try {

        let id = req.params.id

        const dataVoucher = await conn
            .select("*")
            .from("m_ownerlist")
            .where("id", id)

        if (dataVoucher.length > 0) {
            res.status(200).json({
                status: 200,
                data: dataVoucher,
            });
        } else {
            res.status(204).json({
                status: 204,
                data: "Data not found",
            });
        }

    } catch (e) {
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.post("/", async (req, res, next) => {
    try {

        const {
            name_owner,
            id_merchant,
        } = req.body;

        // validate name
        const isNameExsist = await conn("m_ownerlist")
            .select("*")
            .whereRaw(
                `LOWER(name_owner) = ?`,
                name_owner.toLowerCase()
            )
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("id_merchant", id_merchant);
                }
            })
        if (isNameExsist.length > 0) {
            res.status(422).json({
                status: 422,
                message: `${name_owner} is already used`,
            });
            return;
        }

        const respAPIWarehouse = await conn("m_ownerlist")
            .insert({
                name_owner: name_owner,
                id_merchant: id_merchant,
            })
            .returning(["id"]);

        if (respAPIWarehouse.length > 0) {
            console.log("Success Insert data");
            res.status(200).json({
                status: 200,
                data: "Data has been inserted",
            });
        } else {
            console.log("Failed Insert data");
            res.status(500).json({
                status: 500,
                data: "Failed insert data",
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.put("/:id", async (req, res, next) => {
    try {

        let id = req.params.id

        const {
            name_owner,
            id_merchant,
        } = req.body;

        // validate exist email
        const owner = await conn("m_ownerlist").where('id', id);
        const isNameExist = await conn("m_ownerlist")
            .select("*")
            .whereRaw(
                `LOWER(name_owner) = ?`,
                name_owner.toLowerCase()
            )
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("id_merchant", id_merchant);
                }
            })
        if (isNameExist.length > 0 && owner[0].name_owner !== name_owner) {
            res.status(422).json({
                status: 422,
                message: `${name_owner} is already used`,
            });
            return;
        }

        const respPackage = await conn("m_ownerlist")
            .update({
                name_owner: name_owner,
                updated_at:conn.fn.now()
            })
            .where("id", id)
            .returning(["id"]);

        if (respPackage.length > 0) {
            console.log("Success Insert data");
            res.status(200).json({
                status: 200,
                data: "Data has been updated",
            });
        } else {
            console.log("Failed Insert data");
            res.status(500).json({
                status: 500,
                data: "Failed insert data",
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.delete("/", async (req, res, next) => {
    let id = req.body.id;

    try {
        let resp = await conn("m_ownerlist")
            .whereIn("id", id)
            .del();

        res.status(200).json({
            status: 200,
            data: "Success delete data",
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 500,
            data: "Error delete data",
        });
    }
});
