var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");
const voucher_codes = require("voucher-code-generator");

module.exports = router;

router.get("/customer", async (req, res, next) => {

    let id_merchant = req.query.id_merchant
    let id_package = req.query.id_package
    let claim_by = req.query.claim_by

    try {

        const getLastVoucher = await conn
            .select("m_voucher.id", "m_voucher.voucher_code", "m_package.name_package" ,
                "m_voucher.id_batch", "m_batch.name_batch",
                "m_voucher.created_at", "m_voucher.expired_date", "m_voucher.claim_date")
            .from ("m_voucher")
            .where("m_voucher.status", "1")
            .innerJoin("m_batch", "m_voucher.id_batch", "m_batch.id")
            .innerJoin("m_package", "m_voucher.id_package", "m_package.id" )
            .groupBy("m_voucher.id", "m_voucher.voucher_code", "m_package.name_package" ,
                "m_voucher.id_batch", "m_batch.name_batch",
                "m_voucher.created_at", "m_voucher.expired_date")
            .orderBy("m_voucher.expired_date", "asc")
            .limit(1)
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("m_voucher.id_merchant", id_merchant);
                }
                if (id_package) {
                    queryBuilder.where("m_voucher.id_package", id_package);
                }
            })


        const updateStatus = await conn("m_voucher")
            .update({
                id_merchant: id_merchant,
                id_package: id_package,
                claim_date: conn.fn.now(),
                claim_by: claim_by,
                status: 2,
                type: 1,
                updated_at:conn.fn.now()
            })
            .where("voucher_code", getLastVoucher[0].voucher_code)
            .returning(["id"])

        console.log(getLastVoucher)
        console.log(updateStatus)

        if (getLastVoucher.length > 0) {
            res.status(200).json({
                status: 200,
                data: getLastVoucher[0],
            });
        } else {
            res.status(204).json({
                status: 204,
                data: "No Data Found",
            });
        }



    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });

    }

});


router.post("/business", async (req, res, next) => {

    let id_merchant = req.body.id_merchant
    let id_package = req.body.id_package
    let id_batch = req.body.id_batch
    let limit = req.body.limit
    let claim_by = req.body.claim_by

    try {

        const getBisnisVoucher = await conn
            .select("m_voucher.id", "m_voucher.voucher_code", "m_package.name_package" ,
                "m_voucher.id_batch", "m_batch.name_batch",
                "m_voucher.created_at", "m_voucher.expired_date", "m_voucher.claim_date")
            .from ("m_voucher")
            .where("m_voucher.status", "1")
            .innerJoin("m_batch", "m_voucher.id_batch", "m_batch.id")
            .innerJoin("m_package", "m_voucher.id_package", "m_package.id" )
            .groupBy("m_voucher.id", "m_voucher.voucher_code", "m_package.name_package" ,
                "m_voucher.id_batch", "m_batch.name_batch",
                "m_voucher.created_at", "m_voucher.expired_date")
            .orderBy("m_voucher.expired_date", "asc")
            .limit(limit)
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("m_voucher.id_merchant", id_merchant);
                }
                if (id_package) {
                    queryBuilder.where("m_voucher.id_package", id_package);
                }
                if (id_batch) {
                    queryBuilder.where("m_voucher.id_batch", id_batch);
                }
            });

        const claimTrx = await conn.transaction((trx) => {
            const queries = [];

            // bulk insert m_bisnis_history
            const insertHistory = conn("m_bisnis_history")
                .insert({
                    id_merchant,
                    id_batch,
                    id_package,
                    claim_date: conn.fn.now(),
                    claim_by,
                    total_claim: limit,
                    created_at:  conn.fn.now(),
                })
            queries.push(insertHistory);

            // bulk update m_voucher query

            getBisnisVoucher.forEach((voucher) => {
                const query = conn('m_voucher')
                    .where('id', voucher.id)
                    .update({
                        claim_date: conn.fn.now(),
                        claim_by: claim_by,
                        status: 2,
                        updated_at:conn.fn.now(),
                    })
                    .transacting(trx); // This makes every update be in the same transaction
                queries.push(query);
            });

            Promise.all(queries) // Once every query is written
                .then(trx.commit) // We try to execute all of them
                .catch(trx.rollback); // And rollback in case any of them goes wrong
        });

        if (claimTrx.length > 0) {
            res.status(200).json({
                status: 200,
                data: getBisnisVoucher,
            });
        } else {
            res.status(204).json({
                status: 204,
                data: "No Data Found",
            });
        }
    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });

    }

});
