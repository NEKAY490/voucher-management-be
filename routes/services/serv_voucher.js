var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");
var voucher_codes = require('voucher-code-generator');
const { getLocalDate } = require("../../utils");


module.exports = router;


//get Voucher by id merchant & id batch & type (1: customer || 2: bisnis)
router.get("/", async (req, res, next) => {

    let id_merchant = req.query.id_merchant
    let id_batch = req.query.id_batch
    let type = req.query.type
    let status = req.query.status
    let voucher_code = req.query.voucher_code
    let page = +req.query.page || 1
    let per_page = +req.query.per_page || 10

    try {

        const query = conn("m_voucher_view")
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("id_merchant", id_merchant);
                }
                if (id_batch) {
                    queryBuilder.where("id_batch", id_batch);
                }
                if (type) {
                    queryBuilder.where("type", type);
                }
                if (voucher_code) {
                    queryBuilder.whereRaw("lower(voucher_code) like lower(?)", [`%${voucher_code}%`]);
                }
                if (status) {
                    queryBuilder.where("status", status);
                }
            });

        const data = await query
            .clone()
            .select()
            .limit(per_page)
            .offset((page - 1) * per_page)

        const totalCount = await query
            .clone()
            .count()
            .first()

        if (data.length > 0) {
            res.status(200).json({
                status: 200,
                data,
                meta: {
                    count: data.length,
                    current_page: page,
                    per_page,
                    total: +totalCount.count,
                    total_pages: Math.ceil(totalCount.count / per_page),
                },
            });
        } else {
            res.status(204).json({
                status: 204,
                data: {},
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});


router.post("/getVoucherFilter", async (req, res, next) => {

    try {

        const {
            id_merchant,
            voucher_code,
            id_package,
            status,
            start_created_at,
            end_created_at,
            start_expired_date,
            end_expired_date,
            id_batch,
            type,

        } = req.body;

        const dataVoucher = await conn("m_voucher")
            .select("*")
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("id_merchant", id_merchant);
                }
                if (voucher_code) {
                    queryBuilder.where("voucher_code", voucher_code);
                }
                if (id_package) {
                    queryBuilder.where("id_package", id_package);
                }
                if (status) {
                    queryBuilder.where("status", status);
                }
                if (id_batch) {
                    queryBuilder.where("id_batch", id_batch);
                }
                if (type) {
                    queryBuilder.where("type", type);
                }
                if (start_created_at && end_created_at) {
                    queryBuilder.whereRaw("created_at >= ? and created_at <= ?", [start_created_at, end_created_at]);
                }
                if (start_expired_date && end_expired_date) {
                    queryBuilder.whereRaw("expired_date >= ? and expired_date <= ?", [start_expired_date, end_expired_date]);
                }
            })


        const countVoucherAvail = await conn
            .count("status")
            .from("m_voucher")
            .where("status", 1)
            .andWhere("id_batch", id_batch)

        if (dataVoucher.length > 0) {
            res.status(200).json({
                status: 200,
                data: dataVoucher,
                available: countVoucherAvail[0],
            });
        } else {
            res.status(204).json({
                status: 204,
                data: {},
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

//get voucher by voucher code
router.get("/getVoucher", async (req, res, next) => {
    try {

        let voucher_code = req.query.voucher_code

        const dataVoucher = await conn
            .select("m_voucher.*", "m_batch.name_batch", "m_package.name_package", "m_status.name_status", "m_account.name_account")
            .from("m_voucher")
            .innerJoin("m_batch", "m_voucher.id_batch", "m_batch.id")
            .innerJoin("m_package", "m_voucher.id_package", "m_package.id")
            .innerJoin("m_status", "m_voucher.status", "m_status.id")
            .innerJoin("m_account", "m_voucher.create_by", "m_account.id")
            .where("m_voucher.voucher_code", voucher_code)

        if (dataVoucher.length > 0) {
            res.status(200).json({
                status: 200,
                data: dataVoucher,
            });
        } else {
            res.status(204).json({
                status: 204,
                data: "Data not found",
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

//edit voucher_code
router.put("/:voucher_code", async (req, res, next) => {
    try {

        let voucher_code = req.params.voucher_code

        const {
            id_merchant,
            id_package,
            id_role_assign,
            id_batch,
            expired_date,
            claim_date,
            claim_by,
            redeem_date,
            redeem_email,
            status,
            reactive_agent,
            reactive_evidence,
            desc_voucher,
            type,
            update_by
        } = req.body;

        console.log(req.body);

        const respPackage = await conn("m_voucher")
            .update({
                id_merchant: id_merchant,
                id_package: id_package,
                id_role_assign: id_role_assign,
                id_batch: id_batch,
                expired_date: expired_date,
                claim_date: claim_date,
                claim_by: claim_by,
                redeem_date: redeem_date,
                redeem_email: redeem_email,
                status: status,
                reactive_agent: reactive_agent,
                reactive_evidence: reactive_evidence,
                desc_voucher: desc_voucher,
                type: type,
                update_by: update_by,
                updated_at:conn.fn.now()
            })
            .where("voucher_code", voucher_code)
            .returning(["id"])

        if (respPackage.length > 0) {
            console.log(respPackage);
            res.status(200).json({
                status: 200,
                data: "Data has been updated",
            });
        } else {
            console.log(respPackage);
            res.status(500).json({
                status: 500,
                data: "Failed insert data",
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.post("/generate", async (req, res, next) => {

    try {

        const {
            id_merchant,
            id_package,
            name_batch,
            expired_date,
            total_voucher,
            id_role_assign,
            create_by,
            type,

        } = req.body;

        const NameExist = await conn
            .select("*")
            .from("m_batch")
            .andWhere("name_batch", name_batch)

        if (NameExist.length > 0) {
            res.status(409).json({
                status: 409,
                data: "Batch Already Exist",
            });
        } else {

            const respBatch = await conn("m_batch")
                .insert({
                    name_batch: name_batch,
                    total: total_voucher,
                    id_package: id_package,
                    type: type,
                })
                .returning(["id"]);

            const dataVoucher = voucher_codes.generate({
                length: 16,
                count: total_voucher,
                charset: voucher_codes.charset("alphanumeric")
            });

            const baseDataVoucher = {
                id_merchant: id_merchant,
                id_batch: respBatch[0].id,
                id_package: id_package,
                id_role_assign: id_role_assign,
                expired_date: expired_date,
                //voucher_code: dataVoucher[0].toUpperCase(),
                type: type,
                create_by: create_by,
                status: 1,
            }

            console.log(baseDataVoucher)

            const resultData = dataVoucher.map((voucher_code) => ({
                ...baseDataVoucher,
                voucher_code: voucher_code.toUpperCase()
            }))

            console.log(">>>>>>>>>>>>" + resultData)

            const respMVoucher = await conn("m_voucher")
                .insert(
                    resultData
                )


            if (respMVoucher) {
                console.log("Success Insert data");
                res.status(200).json({
                    status: 200,
                    data: resultData,
                });
            } else {
                console.log(respMVoucher);
                res.status(500).json({
                    status: 500,
                    data: "Failed insert data",
                });
            }

        }


    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });

    }

});

router.delete("/:voucher_code", async (req, res, next) => {

    let voucher_code = req.params.voucher_code;

    try {
        let resp = await conn("m_voucher")
            .where("voucher_code", voucher_code)
            .del();

        res.status(200).json({
            status: 200,
            data: "Success delete data",
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 500,
            data: "Error delete data",
        });
    }
});

router.delete("/", async (req, res, next) => {

    let id = req.body.id;

    try {
        let resp = await conn("m_voucher")
            .whereIn("id", id)
            .del();

        res.status(200).json({
            status: 200,
            data: "Success delete data",
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 500,
            data: "Error delete data",
        });
    }
});

//get voucher for customer by id_merchant
router.get("/customer/getAll", async (req, res, next) => {

    let id_merchant = req.query.id_merchant
    let name_package = req.query.name_package
    let name_batch = req.query.name_batch
    let voucher_code = req.query.voucher_code
    let status = req.query.status
    let type = req.query.type
    let page = +req.query.page || 1
    let per_page = +req.query.per_page || 10
    const created_start = req.query.created_start
    const created_end = req.query.created_end
    const expired_start = req.query.expired_start
    const expired_end = req.query.expired_end


    try {

        const query = conn
            .select("m_voucher.id", 
            "m_voucher.voucher_code",
            "m_status.name_status", 
            "m_package.name_package" ,
            "m_voucher.id_batch", 
            "m_batch.name_batch",
                "m_voucher.create_by",
            "m_account.name_account as create_name",
            "m_voucher.created_at",
            "m_voucher.expired_date",
            "m_voucher.claim_date",
            "m_voucher.claim_by",
                "m_account.name_account as claim_name",
            "m_voucher.redeem_date",
            "m_voucher.redeem_email",
            "m_voucher.reactive_agent",
                "m_voucher.reactive_status")
            .from ("m_voucher")
            .innerJoin("m_batch", "m_voucher.id_batch", "m_batch.id")
            .innerJoin("m_package", "m_voucher.id_package", "m_package.id" )
            .innerJoin("m_status", "m_voucher.status", "m_status.id")
            .innerJoin("m_account", "m_voucher.create_by", "m_account.id")
            .groupBy("m_voucher.id", "m_voucher.voucher_code",
                "m_status.name_status", "m_package.name_package" ,
                "m_voucher.id_batch", "m_batch.name_batch",
                "m_voucher.created_at", "m_voucher.expired_date", "m_account.name_account")
            .orderBy("m_voucher.created_at", "desc")
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("m_voucher.id_merchant", id_merchant);
                }
                if (voucher_code) {
                    queryBuilder.whereRaw("lower(voucher_code) like lower(?)", [`%${voucher_code}%`]);
                }
                if (status) {
                    queryBuilder.where("m_voucher.status", status);
                }
                if (type) {
                    queryBuilder.where("m_voucher.type", type);
                }
                if (name_package) {
                    queryBuilder.whereRaw("lower(name_package) like lower(?)", [`%${name_package}%`]);
                }
                if (name_batch) {
                    queryBuilder.whereRaw("lower(name_batch) like lower(?)", [`%${name_batch}%`]);
                }
                if (created_start && created_end) {
                    queryBuilder.whereBetween("m_voucher.created_at", [getLocalDate(created_start), getLocalDate(created_end, 1)]);
                }
                if (expired_start && expired_end) {
                    queryBuilder.whereBetween("m_voucher.expired_date", [getLocalDate(expired_start), getLocalDate(expired_end, 1)]);
                }
            })

        const data = await query
            .clone()
            .limit(per_page)
            .offset((page - 1) * per_page);

        const totalCount = await query;

        res.status(200).json({
            status: 200,
            data,
            meta: {
                count: data.length,
                current_page: page,
                per_page,
                total: totalCount.length,
                total_pages: Math.ceil(totalCount.length / per_page),
            },
        });

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.get("/business/getAll", async (req, res, next) => {

    let id_merchant = req.query.id_merchant
    let name_package = req.query.name_package
    let name_batch = req.query.name_batch
    let type = req.query.type
    const status = +req.query.status;
    const page = +req.query.page || 1
    const per_page = +req.query.per_page || 10
    const created_start = req.query.created_start
    const created_end = req.query.created_end
    const expired_start = req.query.expired_start
    const expired_end = req.query.expired_end

    try {

        const query = conn
            .select(
                "m_voucher.id_batch", "m_voucher.id_package", "m_voucher.id_package", "m_voucher.expired_date",
                "m_account.name_account",
                "m_batch.name_batch",  "m_batch.created_at",
                "m_package.name_package",
                "m_status.name_status"
            )
            .from("m_voucher")
            .innerJoin("m_batch", "m_voucher.id_batch", "m_batch.id")
            .innerJoin("m_package", "m_voucher.id_package", "m_package.id")
            .innerJoin("m_account", "m_voucher.create_by", "m_account.id")
            .innerJoin("m_status", "m_voucher.status", "m_status.code_status")
            .groupBy(
                "m_voucher.id_batch",  "m_voucher.id_package", "m_voucher.expired_date",
                "m_account.name_account",
                "m_batch.id", "m_batch.name_batch",
                "m_package.name_package",
                "m_status.name_status"
            )
            .count('m_status.name_status as total')
            .orderBy("m_batch.created_at", "desc")
            .where("m_batch.type", "2")
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("m_voucher.id_merchant", id_merchant);
                }
                if (status) {
                    queryBuilder.where("m_voucher.status", status);
                }
                if (name_package) {
                    queryBuilder.whereRaw("lower(name_package) like lower(?)", [`%${name_package}%`]);
                }
                if (name_batch) {
                    queryBuilder.whereRaw("lower(name_batch) like lower(?)", [`%${name_batch}%`]);
                }
                if (created_start && created_end) {
                    queryBuilder.whereBetween("m_voucher.created_at", [getLocalDate(created_start), getLocalDate(created_end, 1)]);
                }
                if (expired_start && expired_end) {
                    queryBuilder.whereBetween("m_voucher.expired_date", [getLocalDate(expired_start), getLocalDate(expired_end, 1)]);
                }
            })

        const data = await query
            .clone()
            .limit(per_page)
            .offset((page - 1) * per_page);

        const totalCount = await query

        res.status(200).json({
            status: 200,
            data,
            meta: {
                count: data.length,
                current_page: page,
                per_page,
                total: totalCount.length,
                total_pages: Math.ceil(totalCount.length / per_page),
            },
        });

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.get("/business/count", async (req, res, next) => {

    let id_batch = req.query.id_batch

    try {

        const query = conn
            .select("m_voucher.id_batch", "m_batch.name_batch", "m_voucher.id_package","m_package.name_package", "m_batch.created_at",
                "m_voucher.expired_date", "m_batch.total")
            .from("m_voucher")
            .where("m_batch.type", "2")

        const data = await query
            .clone()
            .limit(per_page)
            .offset((page - 1) * per_page);

        const totalCount = await query
            .clone()
            .count()
            .first()

        res.status(200).json({
            status: 200,
            data,
            meta: {
                count: data.length,
                current_page: page,
                per_page,
                total: totalCount.count,
                total_pages: Math.ceil(totalCount.count / per_page),
            },
        });

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});
