var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");

module.exports = router;

router.get("/", async (req, res, next) => {

    let id_merchant = req.query.id_merchant;
    let api_name = req.query.api_name;
    let api_owner = req.query.api_owner;
    let page = +req.query.page || 1
    let per_page = +req.query.per_page || 10

    try {

        const query = conn("m_api_wrhs")

        const data = await query
            .clone()
            .select()
            .orderBy ("created_at", "desc")
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("id_merchant", id_merchant);
                }
                if (api_name) {
                    queryBuilder.whereRaw("lower(api_name) like lower(?)", [`%${api_name}%`]);
                }
                if (api_owner) {
                    queryBuilder.where("api_owner", api_owner);
                }

            })
            .limit(per_page)
            .offset((page - 1) * per_page)

        const totalCount = await query
            .clone()
            .count()
            .first()

        res.status(200).json({
            status: 200,
            data,
            meta: {
                count: data.length,
                current_page: page,
                per_page,
                total: +totalCount.count,
                total_pages: Math.ceil(totalCount.count / per_page),
            },
        });

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.get("/", async (req, res, next) => {
    try {
        const dataAPI = await conn
            .select("*")
            .orderBy ("created_at", "desc")
            .from("m_api_wrhs")

        if (dataAPI.length > 0) {
            res.status(200).json({
                status: 200,
                data: dataAPI,
            });
        } else {
            res.status(204).json({
                status: 204,
                data: {},
            });
        }

    } catch (e) {
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.get("/:id", async (req, res, next) => {
    try {

        let id = req.params.id

        const dataVoucher = await conn
            .select("*")
            .from("m_api_wrhs")
            .where("id", id)

        if (dataVoucher.length > 0) {
            res.status(200).json({
                status: 200,
                data: dataVoucher,
            });
        } else {
            res.status(204).json({
                status: 204,
                data: "Data not found",
            });
        }

    } catch (e) {
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.post("/", async (req, res, next) => {
    try {

        const {
            api_url,
            api_owner,
            desc_package,
            desc_api,
            api_name,
            header,
            body,
            method_name,
            flag_auth,
            id_merchant,
        } = req.body;

        // validate name
        const isNameExsist = await conn("m_api_wrhs")
            .select("*")
            .whereRaw(
                `LOWER(api_name) = ?`,
                api_name.toLowerCase()
            )
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("id_merchant", id_merchant);
                }
            })
        if (isNameExsist.length > 0) {
            res.status(422).json({
                status: 422,
                message: `${api_name} is already used`,
            });
            return;
        }

            const respAPIWarehouse = await conn("m_api_wrhs")
                .insert({
                    api_url: api_url,
                    api_owner: api_owner,
                    desc_package: desc_package,
                    desc_api: desc_api,
                    api_name: api_name,
                    header: header,
                    body: body,
                    method_name: method_name,
                    flag_auth: flag_auth,
                    id_merchant: id_merchant,
                })
                .returning(["id"]);

            if (respAPIWarehouse.length > 0) {
                console.log("Success Insert data");
                res.status(200).json({
                    status: 200,
                    data: "Data has been inserted",
                });
            } else {
                console.log("Failed Insert data");
                res.status(500).json({
                    status: 500,
                    data: "Failed insert data",
                });
            }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.put("/:id", async (req, res, next) => {
    try {

        let id = req.params.id

        const {
            api_url,
            api_owner,
            desc_package,
            desc_api,
            api_name,
            header,
            method_name,
            body,
            flag_auth,
        } = req.body;

        // validate exist email
        const apiWarehouse = await conn("m_api_wrhs").where('id', id);
        const isNameExist = await conn("m_api_wrhs")
            .select("*")
            .whereRaw(
                `LOWER(api_name) = ?`,
                api_name.toLowerCase()
            )
            .modify(function (queryBuilder) {
                queryBuilder.where("id_merchant", apiWarehouse[0].id_merchant);
            })
        if (isNameExist.length > 0 && apiWarehouse[0].api_name !== api_name) {
            res.status(422).json({
                status: 422,
                message: `${api_name} is already used`,
            });
            return;
        }

        const respPackage = await conn("m_api_wrhs")
            .update({
                api_url: api_url,
                api_owner: api_owner,
                desc_package: desc_package,
                desc_api: desc_api,
                api_name: api_name,
                header: header,
                method_name: method_name,
                body: body,
                flag_auth: flag_auth,
                updated_at:conn.fn.now()
            })
            .where("id", id)
            .returning(["id"]);

        if (respPackage.length > 0) {
            console.log("Success Insert data");
            res.status(200).json({
                status: 200,
                data: "Data has been updated",
            });
        } else {
            console.log("Failed Insert data");
            res.status(500).json({
                status: 500,
                data: "Failed insert data",
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.delete("/", async (req, res, next) => {
    let id = req.body.id;

    try {
        let resp = await conn("m_api_wrhs")
            .del()
            .whereIn("id", id)

        res.status(200).json({
            status: 200,
            data: "Success delete data",
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 500,
            data: "Error delete data",
        });
    }
});
