var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");

module.exports = router;

router.get("/customer", async (req, res, next) => {

    let id_merchant = req.query.id_merchant
    const page = +req.query.page || 1
    const per_page = +req.query.per_page || 10


    try {

        const query = conn
            .select("m_voucher.id", "m_voucher.voucher_code", "m_package.name_package" ,
                "m_voucher.id_batch", "m_batch.name_batch",
                "m_voucher.created_at", "m_voucher.expired_date", "m_voucher.claim_date")
            .from ("m_voucher")
            .whereRaw("m_voucher.claim_date is not null")
            .innerJoin("m_batch", "m_voucher.id_batch", "m_batch.id")
            .innerJoin("m_package", "m_voucher.id_package", "m_package.id" )
            .groupBy("m_voucher.id", "m_voucher.voucher_code", "m_package.name_package" ,
                "m_voucher.id_batch", "m_batch.name_batch",
                "m_voucher.created_at", "m_voucher.expired_date", "m_voucher.claim_date")
            .orderBy("m_voucher.claim_date", "desc")
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("m_voucher.id_merchant", id_merchant);
                }
            })

        const data = await query
            .clone()
            .limit(per_page)
            .offset((page - 1) * per_page);

        const totalCount = await query

        res.status(200).json({
            status: 200,
            data,
            meta: {
                count: data.length,
                current_page: page,
                per_page,
                total: totalCount.length,
                total_pages: Math.ceil(totalCount.length / per_page),
            },
        });

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.get("/bisnis", async (req, res, next) => {

    let id_merchant = req.query.id_merchant
    const page = +req.query.page || 1
    const per_page = +req.query.per_page || 10


    try {

        const query = conn
            .select("m_bisnis_history.*", "m_batch.name_batch", "m_package.name_package", "m_account.name_account")
            .from("m_bisnis_history")
            .join("m_batch", "m_bisnis_history.id_batch", "m_batch.id")
            .join("m_package", "m_bisnis_history.id_package", "m_package.id")
            .join("m_account", "m_bisnis_history.claim_by", "m_account.id")
             .groupBy("m_bisnis_history.id", "m_batch.name_batch", "m_package.name_package", "m_account.name_account")
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("m_bisnis_history.id_merchant", id_merchant);
                }
            })

        const data = await query
            .clone()
            .select()
            .limit(per_page)
            .offset((page - 1) * per_page);

        const totalCount = await query

        res.status(200).json({
            status: 200,
            data,
            meta: {
                count: data.length,
                current_page: page,
                per_page,
                total: totalCount.length,
                total_pages: Math.ceil(totalCount.length / per_page),
            },
        });

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.get("/detail/bisnis", async (req, res, next) => {

    let id_batch = req.query.id_batch
    let claim_date = req.query.claim_date
    const page = +req.query.page || 1
    const per_page = +req.query.per_page || 10


    try {

        const query = conn.from("m_voucher")
            .modify(function (queryBuilder) {
                if (id_batch) {
                    queryBuilder.where("id_batch", id_batch);
                }
                if (claim_date) {
                    queryBuilder.where("claim_date", claim_date);
                }
            })

        const data = await query
            .clone()
            .select()
            .limit(per_page)
            .offset((page - 1) * per_page);

        const totalCount = await query
            .clone()
            .count()
            .first()


        res.status(200).json({
            status: 200,
            data,
            meta: {
                count: data.length,
                current_page: page,
                per_page,
                total: totalCount.count,
                total_pages: Math.ceil(totalCount.count / per_page),
            },
        });

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});
