var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");

module.exports = router;


router.get("/", async (req, res, next) => {
    try {
        const dataUser = await conn
            .select("*")
            .from("m_status")

        if (dataUser.length > 0) {
            res.status(200).json({
                status: 200,
                data: dataUser,
            });
        } else {
            res.status(204).json({
                status: 204,
                data: {},
            });
        }

    } catch (e) {
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

