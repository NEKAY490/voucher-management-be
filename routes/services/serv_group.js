var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");

module.exports = router;

router.get("/", async (req, res, next) => {

    let id_merchant = req.query.id_merchant;
    let page = +req.query.page || 1
    let per_page = +req.query.per_page || 10

    try {
        const query = conn("m_group")


        const data = await query
            .clone()
            .select()
            .orderBy ("created_at", "desc")
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("id_merchant", id_merchant);
                }
            })
            .limit(per_page)
            .offset((page - 1) * per_page)

        const totalCount = await query
            .clone()
            .count()
            .first()

        res.status(200).json({
            status: 200,
            data,
            meta: {
                count: data.length,
                current_page: page,
                per_page,
                total: totalCount.count,
                total_pages: Math.ceil(totalCount.count / per_page),
            },
        });

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.get("/:id", async (req, res, next) => {
    try {

        let id = req.params.id

        const dataGroup = await conn
            .select("*")
            .from("m_group")
            .where("id", id)

        if (dataGroup.length > 0) {
            res.status(200).json({
                status: 200,
                data: dataGroup,
            });
        } else {
            res.status(204).json({
                status: 204,
                data: "Data not found",
            });
        }

    } catch (e) {
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.post("/", async (req, res, next) => {
    try {

        const {
            id_merchant,
            name_group,
        } = req.body;

        const dataGroup = await conn("m_group")
            .insert({
                id_merchant: id_merchant,
                name_group: name_group,
            })
            .returning(["id"]);

        const masterGroup = await conn("m_usergroup")
            .insert({
                id_merchant: id_merchant,
                id_group: dataGroup[0].id,
                id_module: id_module,
                fcreate: 1,
                fread: 1,
                fupdate: 1,
                fdelete:  1
            })
            .returning(["id"]);

        if (dataGroup.length > 0) {
            console.log("Success Insert data");
            res.status(200).json({
                status: 200,
                data: "Data has been inserted",
            });
        } else {
            console.log("Failed Insert data");
            res.status(500).json({
                status: 500,
                data: "Failed insert data",
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

