var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");

module.exports = router;

router.get("/customer", async (req, res, next) => {

    let id_merchant = req.query.id_merchant
    let id_batch = req.query.id_batch
    let id_package = req.query.id_package

    try {

        const countVoucherAvail = await conn
            .count("status")
            .from("m_voucher")
            .where("status", 1)
            .where("type", 1)
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("id_merchant", id_merchant);
                }
                if (id_package) {
                    queryBuilder.where("id_package", id_package);
                }
                if (id_batch) {
                    queryBuilder.where("id_package", id_batch);
                }
            })

        if (countVoucherAvail.length > 0) {
            res.status(200).json({
                status: 200,
                available: countVoucherAvail[0],
            });
        } else {
            res.status(204).json({
                status: 204,
                data: {},
            });
        }

    } catch (e) {
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.get("/bisnis", async (req, res, next) => {

    let id_merchant = req.query.id_merchant
    let id_batch = req.query.id_batch
    let id_package = req.query.id_package

    try {

        const data = await conn
            .count("status as quota")
            .from("m_voucher")
            .where("status", 1)
            .where("type", 2)
            .where("id_package", id_package)
            .where("id_batch", id_batch)
            .modify(function (queryBuilder) {
                if (id_merchant) {
                    queryBuilder.where("id_merchant", id_merchant);
                }
            })

        if (data.length > 0) {
            res.status(200).json({
                status: 200,
                data: data[0],
            });
        } else {
            res.status(204).json({
                status: 204,
                data: {},
            });
        }

    } catch (e) {
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});
