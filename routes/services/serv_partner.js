var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");
const voucher_codes = require("voucher-code-generator");
const { getLocalDate } = require("../../utils");

module.exports = router;


router.post("/generate", async (req, res, next) => {

    try {
        const { name_batch, vouchers, id_merchant, owner, name_package, create_by, name_file, url_file, expired_date } = req.body;
        const total = vouchers.length
        // const owner = vouchers[0]['Owner']
        // const name_package = vouchers[0]['Package']

        // check existing batch by name
        const isBatchExist = await conn('m_batch').where("name_batch", name_batch)
        if (isBatchExist.length > 0) {
            res.status(409).json({
                status: 409,
                data: "Batch Already Exist",
            });
        }

        // create package
        const package = await conn("m_package")
            .insert({
                name_package,
                id_merchant,
            })
            .returning("*");

        // insert batch
        const batch = await conn("m_batch")
            .insert({
                name_batch,
                total,
                type: 3,
                owner,
                name_file,
                url_file,
                id_package: package[0].id,
            })
            .returning("*")


        const dateToTimestamp = (date) => {
            const dateArray = date.split("-")
            return new Date(`${dateArray[2]}-${dateArray[1]}-${dateArray[0]}`);
        }

        // transform data
        const transformedVouchers = vouchers.map((voucher) => ({
            id_package: package[0].id,
            id_batch: batch[0].id,
            voucher_code: voucher['Voucher Code'],
            // expired_date: dateToTimestamp(voucher.Expired),
            expired_date: dateToTimestamp(expired_date),
            status: 1,
            type: 3,
            create_by: create_by,
            id_merchant: id_merchant,
        }))

        // insert vouchers bulk
        const result = await conn("m_voucher")
            .insert(transformedVouchers)
            .returning("*")


        res.status(200).json({
            status: 200,
            data: result,
        });
    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });

    }

});

router.get("/", async (req, res, next) => {
    try {

        const page = +req.query.page || 1
        const per_page = +req.query.per_page || 10
        let id_merchant = req.query.id_merchant
        let name_package = req.query.name_package
        let name_batch = req.query.name_batch

        const { created_start, created_end, expired_start, expired_end } = req.query;

        const query = conn
            .select("m_voucher.id_batch", "m_batch.name_batch", "m_batch.name_file", "m_batch.owner", "m_package.name_package", "m_batch.created_at",
                "m_voucher.expired_date", "m_batch.total")
            .from("m_voucher")
            .innerJoin("m_batch", "m_voucher.id_batch", "m_batch.id")
            .innerJoin("m_package", "m_voucher.id_package", "m_package.id")
            .groupBy("m_voucher.id_batch",  "m_batch.id", "m_batch.name_batch", "m_package.name_package", "m_voucher.expired_date")
            .orderBy("m_batch.created_at", "desc")
            .where("m_batch.type", "3")
            .modify(function (queryBuilder) {
                if (created_start && created_end) {
                    queryBuilder.whereBetween("m_batch.created_at", [getLocalDate(created_start), getLocalDate(created_end, 1)]);
                }

                if (id_merchant) {
                    queryBuilder.where("m_voucher.id_merchant", id_merchant);
                }
                if (name_package) {
                    queryBuilder.whereRaw("lower(name_package) like lower(?)", [`%${name_package}%`]);
                }
                if (name_batch) {
                    queryBuilder.whereRaw("lower(name_batch) like lower(?)", [`%${name_batch}%`]);
                }
            })

        const data = await query
            .clone()
            .limit(per_page)
            .offset((page - 1) * per_page)

        res.status(200).json({
            status: 200,
            data,
            meta: {
                count: data.length,
                current_page: page,
                per_page,
                total: data.length,
                total_pages: Math.ceil(data.length / per_page),
            },
        });

    } catch (e) {
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.put("/:id", async (req, res, next) => {
    try {

        let id = req.params.id

        const {
            name_batch,
            total,
        } = req.body;

        const respPackage = await conn("m_batch")
            .update({
                name_batch: name_batch,
                total: total,
                updated_at:conn.fn.now()
            })
            .where("id", id)
            .returning(["id"]);

        if (respPackage.length > 0) {
            console.log("Success Insert data");
            res.status(200).json({
                status: 200,
                data: "Data has been updated",
            });
        } else {
            console.log("Failed Insert data");
            res.status(500).json({
                status: 500,
                data: "Failed insert data",
            });
        }

    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }
});

router.delete("/", async (req, res, next) => {
    let id = req.body.id;

    try {
        let resp = await conn("m_batch")
            .whereIn("id", id)
            .del();

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 500,
            data: "Error delete data",
        });
    }
});
