const router = require("express").Router();
const member = require("../controllers/member.controller");

router.post("/", member.createValidate(), member.create);
router.get("/", member.findAll);
router.get("/:id", member.findById);
router.put("/:id", member.updateValidate(), member.update);
router.delete("/", member.delete);

module.exports = router;