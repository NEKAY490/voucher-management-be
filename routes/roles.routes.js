const router = require("express").Router();
const roles = require("../controllers/roles.controller");

router.post("/", roles.validate(), roles.create);
router.get("/", roles.findAll);
router.get("/:id", roles.findById);
router.put("/:id", roles.update);
router.delete("/", roles.delete);

module.exports = router;