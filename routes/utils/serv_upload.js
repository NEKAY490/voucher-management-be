var express = require('express');
var router = express.Router();
const conn = require("../../connection/db");
const path = require("path");
const multer = require('multer');// Create multer object

module.exports = router;

//const upload = multer();
const imageUpload = multer({
    dest: './public/fileupload/',
});

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/fileupload/')
    },
    filename: function (req, file, cb) {
        let extArray = file.mimetype.split("/");
        let extension = extArray[extArray.length - 1];
        cb(null, file.fieldname + '-' + Date.now()+ '.' +extension)
    }
})

var upload = multer({ storage: storage });

router.post('/upload',  upload.single("file", 12),  (req, res) => {

    try {
        if (!req.files.empty) {
            console.log(req.files);
            res.status(200).json({
                status: 200,
                data: req.files,
            });
        } else {
            res.status(500).json({
                status: 500,
                data: "Error",
            });
        }


    } catch (e) {
        console.log(e)
        res.status(500).json({
            status: 500,
            data: "Internal Server Error",
        });
    }


});

// Image Get Routes
router.get('/upload/:filename', (req, res) => {
    console.log("get file")
    const { filename } = req.params;
    const dirname = path.resolve();
    const fullfilepath = path.join(dirname, './public/fileupload/' + filename);
    return res.sendFile(fullfilepath);

});
