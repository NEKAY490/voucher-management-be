const { body, validationResult } = require("express-validator");
const knex = require("../connection/db");

// validation form
exports.validate = () => {
  return [body("name").notEmpty()];
};

// get fullname module
const rolesTransform = async (roles) => {
  const modules = await knex("m_modules");

  return roles.map((role) => {
    let modules_name = "";

    if (role.modules_id) {
      modules_name = role.modules_id
        .split(",")
        .map((id) => modules.find((module) => module.id == id).name)
        .join(",");
    }

    return {
      ...role,
      modules_name,
    };
  });
};

exports.create = async (req, res) => {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(422).json({
        status: 422,
        message: errors.array(),
      });
      return;
    }

    const { name, modules_id, id_merchant } = req.body;

    // validate role name
    const isNameExsist = await knex("m_roles")
      .where("id_merchant", id_merchant)
      .whereRaw(`LOWER(name) = ?`, name.toLowerCase());

    if (isNameExsist.length > 0) {
      res.status(422).json({
        status: 422,
        message: `Role ${name} is already used`,
      });
      return;
    }

    const data = await knex("m_roles")
      .insert({
        name,
        modules_id,
        id_merchant,
      })
      .returning(["name", "modules_id", "id_merchant"]);

    res.status(200).json({
      status: 200,
      data,
    });
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err.message || "Some error occurred while creating new Role.",
    });
  }
};

exports.findAll = async (req, res) => {
  try {
    const { name, id_merchant, page, per_page } = req.query;

    const query = knex("m_roles");

    if (name) {
      query.whereRaw(`LOWER(name) LIKE ?`, [`%${name.toLowerCase()}%`]);
    }
    if (id_merchant) {
      query.where("id_merchant", id_merchant);
    }

    const roles = await query
      .clone()
      .select("*")
      .whereNull("deleted_at")
      .orderBy("created_at", "desc")
      .limit(per_page)
      .offset((page - 1) * per_page);
    const data = await rolesTransform(roles);

    const totalCount = await query.clone().count().first();

    res.status(200).json({
      status: 200,
      data,
      meta: {
        count: data.length,
        current_page: page || 1,
        per_page,
        total: totalCount.count,
        total_pages: Math.ceil(totalCount.count / per_page) || 1,
      },
    });
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err.message || "Some error occurred while get Roles data.",
    });
  }
};

exports.findById = async (req, res) => {
  const id = req.params.id;

  try {
    const roles = await knex("m_roles").where({ id }).whereNull("deleted_at");

    if (roles.length < 1) {
      res.status(400).json({
        status: 400,
        message: `Role with id=${id} was not found!`,
      });
    }

    const data = await rolesTransform(roles);

    res.status(200).json({
      status: 200,
      data,
    });
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: `Error retrieving Role with id=${id}`,
    });
  }
};

exports.update = async (req, res) => {
  const id = req.params.id;

  try {
    const { name, modules_id, id_merchant } = req.body;

    // validate role name
    const role = await knex("m_roles").where('id', id);
    const isNameExsist = await knex("m_roles")
      .where("id_merchant", id_merchant)
      .whereRaw(`LOWER(name) = ?`, name.toLowerCase());

    if (isNameExsist.length > 0 && role[0].name !== name) {
      res.status(422).json({
        status: 422,
        message: `Role ${name} is already used`,
      });
      return;
    }

    const data = await knex("m_roles").where({ id }).update(
      {
        name,
        modules_id,
        id_merchant,
      },
      ["id", "name", "modules_id", "id_merchant"]
    );

    if (data.length < 1) {
      res.status(400).json({
        status: 400,
        message: `Cannot update Role with id=${id}. Maybe Role was not found!`,
      });
    }

    res.status(200).json({
      status: 200,
      message: "Role was updated successfully.",
      data,
    });
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: `Error updating Role with id=${id}`,
    });
  }
};

exports.delete = async (req, res) => {
  const id = req.body.id;

  try {
    const data = await knex("m_roles").whereIn("id", id).update({
      deleted_at: knex.fn.now(),
    });

    if (data.length < 1) {
      res.status(400).json({
        status: 400,
        message: `Cannot delete Role with id=${id}. Maybe Role was not found!`,
      });
    }

    res.status(200).json({
      status: 200,
      message: "Role was deleted successfully.",
    });
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: `Could not delete Role with id=${id}`,
    });
  }
};
