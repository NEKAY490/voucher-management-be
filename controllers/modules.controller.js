const knex = require("../connection/db");

exports.findAll = async (_, res) => {
  try {
    const data = await knex("m_modules").orderBy('id', 'asc');
    res.status(200).json({
      status: 200,
      data,
    });
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err.message || "Some error occurred while get Modules data.",
    });
  }
};