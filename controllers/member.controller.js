const { body, validationResult } = require("express-validator");
const knex = require("../connection/db");
const { getAccountFromToken, geEncryptedPassword } = require("../utils");

// validation form
exports.createValidate = () => {
  return [
    body("name_account").notEmpty(),
    body("mail_account").isEmail(),
    body("password").isLength({ min: 5 }),
    body("id_group").notEmpty(),
  ];
};

exports.updateValidate = () => {
  return [
    body("name_account").notEmpty(),
    body("mail_account").isEmail(),
    body("id_group").notEmpty(),
  ];
};

const membersTransform = async (members) => {
  const roles = await knex("m_roles");

  return members.map((member) => {
    const role = roles.find((role) => role.id == member.id_group);

    return {
      ...member,
      role_name: role ? role.name : "",
    };
  });
};

exports.create = async (req, res) => {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(422).json({
        status: 422,
        message: errors.array(),
      });
      return;
    }

    const account = await getAccountFromToken(req);
    const { name_account, mail_account, password, id_group } = req.body;

    // validate email
    const isNameExsist = await knex("m_account")
      .whereRaw(
        `LOWER(mail_account) = ?`,
        mail_account.toLowerCase()
      );

    if (isNameExsist.length > 0) {
      res.status(422).json({
        status: 422,
        message: `Email ${mail_account} is already used`,
      });
      return;
    }

    const data = await knex("m_account")
      .insert({
        name_account,
        mail_account,
        password: geEncryptedPassword(password),
        id_group,
        account_status: 0,
        created_by: account.id,
        id_merchant: account.id_merchant,
      })
      .returning(["*"]);

    res.status(200).json({
      status: 200,
      data,
    });
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err.message || "Some error occurred while creating new Member.",
    });
  }
};

exports.findAll = async (req, res) => {
  try {
    const { name, page, per_page, id_merchant } = req.query;
    const account = await getAccountFromToken(req);
    const idMerchant = id_merchant || account.id_merchant;

    const query = knex("m_account").where("id_merchant", idMerchant);
    if (name) {
      query.whereRaw(`LOWER(name_account) LIKE ?`, [`%${name.toLowerCase()}%`]);
    }
    const totalCount = await query.clone().count().first();

    const member = await query
      .clone()
      .select(
        "id",
        "name_account",
        "mail_account",
        "phone_account",
        "account_status",
        "created_at",
        "updated_at",
        "id_group",
        "id_merchant"
      )
      .orderBy("created_at", "desc")
      .limit(per_page)
      .offset((page - 1) * per_page);
    const data = await membersTransform(member);

    res.status(200).json({
      status: 200,
      data,
      meta: {
        count: data.length,
        current_page: page || 1,
        per_page,
        total: totalCount.count,
        total_pages: Math.ceil(totalCount.count / per_page) || 1,
      },
    });
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err.message || "Some error occurred while get Members data.",
    });
  }
};

exports.findById = async (req, res) => {
  const id = req.params.id;

  try {
    const member = await knex("m_account")
      .select(
        "id",
        "name_account",
        "mail_account",
        "phone_account",
        "account_status",
        "created_at",
        "updated_at",
        "id_group",
        "id_merchant"
      )
      .where({ id });

    if (member.length < 1) {
      res.status(400).json({
        status: 400,
        message: `Member with id=${id} was not found!`,
      });
    }

    const data = await membersTransform(member);

    res.status(200).json({
      status: 200,
      data: data[0],
    });
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: `Error retrieving Member with id=${id}`,
    });
  }
};

exports.update = async (req, res) => {
  const id = req.params.id;

  try {
    const id = req.params.id;
    const { name_account, mail_account, password, id_group, account_status } =
      req.body;

    // validate wrong id
    const member = await knex("m_account").where('id', id);
    if (member.length < 1) {
      res.status(422).json({
        status: 422,
        message: `id ${id} was not found`,
      });
      return;
    }

    // validate exist email
    const isEmailExist = await knex("m_account").where('mail_account', mail_account);
    if (isEmailExist.length > 0 && member[0].mail_account !== mail_account) {
      res.status(422).json({
        status: 422,
        message: `Email ${mail_account} is already used`,
      });
      return;
    }

    const account = await getAccountFromToken(req);

    const payload = {
      name_account,
      mail_account,
      id_group,
      account_status,
      updated_by: account.id,
    };
    if (password) {
      payload.password = geEncryptedPassword(password);
    }

    const data = await knex("m_account")
      .where({ id })
      .update(payload)
      .returning(["*"]);

    if (data.length < 1) {
      res.status(400).json({
        status: 400,
        message: `Cannot update Member with id=${id}. Maybe Member was not found!`,
      });
    }

    res.status(200).json({
      status: 200,
      message: "Member was updated successfully.",
      data,
    });
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: `Error updating Member with id=${id}`,
    });
  }
};

exports.delete = async (req, res) => {
  const id = req.body.id;

  try {
    const data = await knex("m_account").whereIn("id", id).del();

    if (data.length < 1) {
      res.status(400).json({
        status: 400,
        message: `Cannot delete Member with id=${id}. Maybe Member was not found!`,
      });
    }

    res.status(200).json({
      status: 200,
      message: "Member was deleted successfully.",
    });
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: `Could not delete Member with id=${id}`,
    });
  }
};
