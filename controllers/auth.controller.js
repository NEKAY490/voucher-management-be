const { body } = require("express-validator");
const knex = require("../connection/db");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config = require("../config.json");
const { getModulesById } = require("../utils");

exports.validate = () => {
  return [body("email").notEmpty().isEmail(), body("password").notEmpty()];
};

exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;

    // check email
    const account = await knex("m_account")
      .where({ mail_account: email })
      .first();
    if (account === undefined) {
      res.status(400).json({
        status: 400,
        message: "User was not found!",
      });
    }

    // check password
    const isPasswordValid = bcrypt.compareSync(password, account.password);
    if (!isPasswordValid) {
      res.status(403).json({
        status: 403,
        message: "Wrong password",
      });
    }

    // generate token
    const payload = {
      id: account.id,
      name: account.name_account,
      email: account.mail_account,
      id_group: account.id_group,
      id_merchant: account.id_merchant,
    };
    const expiresIn = 60 * 60 * 24 * 15; // 15 days
    const token = jwt.sign(payload, config.jwtSecret, { expiresIn });

    res.status(200).json({
      token,
      expires_in: expiresIn,
    });
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err.message || "Some error occurred while login Account.",
    });
  }
};

exports.verify = async (req, res) => {
  try {
    const { token } = req.body;
    const account = jwt.verify(token, config.jwtSecret);
    const modules = await getModulesById(account.id_group);

    // generate token
    const payload = {
      ...account,
      modules,
    };
    res.status(200).json(payload);
  } catch (err) {
    res.status(500).json({
      status: 500,
      message: err.message || "Some error occurred while verify Account.",
    });
  }
};
