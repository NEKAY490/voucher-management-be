const knex = require("../connection/db");
const jwt = require('jsonwebtoken');
const config = require("../config.json");
const bcrypt = require("bcrypt");

exports.getModulesById = async (id) => {
  // get role by group id account
  const role = await knex("m_roles").where({ id }).first();

  // get list module
  const modules = await knex("m_modules");

  return role.modules_id
    .split(",")
    .map((id) => modules.find((module) => module.id == id));
};

exports.getAccountFromToken = (req) => {
  const token = req.header('Authorization').split('Bearer ')[1];
  return jwt.verify(token, config.jwtSecret);
}

exports.geEncryptedPassword = (password) => {
  const salt = bcrypt.genSaltSync(10);
  return bcrypt.hashSync(password, salt);
}


exports.getLocalDate = (date, dateAdded = 0) => {
  var result = new Date(date);
  result.setDate(result.getDate() + dateAdded);
  return result;
}

// logging
exports.getLogDescription = (method, url, body) => {
  const methodGlossary = {
    POST: 'Create',
    PUT: 'Update',
    DELETE: 'Delete',
  }

  const description = url.split('/')[3];
  const naming = body.name;

  return `${methodGlossary[method]} ${description} ${naming}`;
}